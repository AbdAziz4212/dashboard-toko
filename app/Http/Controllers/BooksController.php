<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;
use Spatie\Permission\Models\Role;

use App\Category;
use App\Book;

use DB;
use Hash;
use Image;
use DataTables;
use Form;

class BooksController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:books-list', ['only' => ['index', 'show']]);
        $this->middleware('permission:books-create', ['only' => ['create', 'store']]);
        $this->middleware('permission:books-edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:books-delete', ['only' => ['destroy']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $books = Book::orderBy('created_at', 'ASC')->get();
        return view('backend.books.index', compact('books'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::all();
        return view('backend.books.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name'  => 'required',
            'type'  => 'required',
            'price' => 'required',
        ]);

        Book::create([
            'category_id' => $request->type,
            'name' => $request->name,
            'price' => str_replace(',', '' ,$request->price),
        ]);

        return redirect()->route('books.index')
                        ->with('success', 'Buku berhasil ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $categories = Category::all();
        $book = Book::findOrFail($id);
        return view('backend.books.edit', compact('categories','book'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name'  => 'required',
            'type'  => 'required',
            'price' => 'required',
        ]);

        $book = Book::findOrFail($id);

        $book->update([
            'category_id' => $request->type,
            'name'        => $request->name,
            'price'       => str_replace(',', '' ,$request->price),
        ]);

        return redirect()->route('books.index')
                        ->with('success', 'Buku berhasil diupdate');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $book = Book::findOrFail($id);
        $book->delete();
        return response()->json(['status' => true, 'message' => 'Buku berhasil dihapus']);
    }
}
