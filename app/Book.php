<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    protected $table = 'books';
    protected $fillable = [
        'category_id',
        'name',
        'price'
    ];

    public function dataCategory() {
        return $this->belongsTo('App\Category', 'category_id', 'id');
    }
}
