@extends('layouts.app')

@section('title', 'Dashboard')

@section('styles')
<link rel="stylesheet" href="{{ asset('modules/fotorama/fotorama.css') }}">
<link rel="stylesheet" href="{{ asset('modules/select2/css/select2.min.css') }}">
@endsection

@section('content')
<div class="section-header border-top">
    <h1>Dashboard</h1>
</div>

<div class="section-body">
    <h2 class="section-title">
        Selamat datang di dashboard
        <span class="text-primary">MT.com</span>
    </h2>
</div>

<div class="row">

    <div class="col-lg-3 col-md-6 col-sm-6 col-12">
        <div class="card card-statistic-1">
            <div class="card-icon bg-primary">
                <i class="fas fa-book"></i>
            </div>
            <div class="card-wrap">
                <div class="card-header">
                <h4>Total Buku</h4>
                </div>
                <div class="card-body">
                    10
                </div>
            </div>
        </div>
    </div>

    <div class="col-lg-3 col-md-6 col-sm-6 col-12">
        <div class="card card-statistic-1">
            <div class="card-icon bg-success">
                <i class="fas fa-list-ul"></i>
            </div>
            <div class="card-wrap">
                <div class="card-header">
                <h4>Total Kategori</h4>
                </div>
                <div class="card-body">
                    10
                </div>
            </div>
        </div>
    </div>

</div>
@endsection

@section('javascript')

@endsection
