@extends('layouts.app')

@section('title', 'Kategori Buku')

@section('styles')
<link rel="stylesheet" href="{{ asset('modules/datatables/datatables.min.css') }}">
<link rel="stylesheet" href="{{ asset('modules/datatables/datatables/css/dataTables.bootstrap4.min.css') }}">
@endsection

@section('content')
<div class="section-header">
    <h1>Kategori Buku</h1>

    @can('categories-create')
        <div class="section-header-button ml-auto">
            <a class="btn btn-primary" href="{{ route('categories.create') }}">
                Buat Kategori Buku
            </a>
        </div>
    @endcan
</div>

<div class="section-body">

    <div class="row">
        <div class="col-12">

            <div class="card">
                <div class="card-body">

                    <div class="table-responsive">
                        <table class="table table-striped" id="table-data">
                            <thead>
                                <tr>
                                    <th width="5%">#</th>
                                    <th>Nama Kategori</th>
                                    <th width="20%">&nbsp;</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($categories as $category)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $category->name }}</td>
                                        <td>
                                            <div class="btn-toolbar" role="toolbar">
                                                <div class="btn-group mr-2" role="group">
                                                    <a class="btn btn-primary" href="{{ route('categories.edit', $category->id) }}">Edit</a>
                                                </div>
                                                <div class="btn-group">
                                                    {{ Form::button('Delete', ['id' => 'button-delete-'. $category->id, 'class' => 'btn btn-danger', 'data-route' => route('categories.destroy', $category->id) , 'onclick' => 'delete_data('. $category->id .')']) }}
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>

                        </table>
                    </div>
                </div>
            </div>

        </div>
    </div>

</div>
@endsection

@section('javascript')
    <script src="{{ asset('modules/datatables/datatables/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('modules/datatables/datatables/js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('modules/sweetalert/sweetalert.min.js') }}"></script>
    <script type="text/javascript">
    var tableData;
    $(function() {
        @if ($message = Session::get('success'))
            toastr.success('{{ $message }}', 'Success');
        @endif

        tableData = $('#table-data').DataTable();

    });

    function delete_data(id)
    {
        var formUrl = $('#button-delete-'+ id).data('route');
        swal({
            title: 'Hapus Data?',
            text: 'Apakah yakin menghapus data?',
            buttons: {
                cancel: true,
                confirm: {
                    text: "Hapus!",
                    closeModal: false,
                }
            },
            dangerMode: true,
            closeOnClickOutside: false
        })
        .then((willDelete) => {
            if (willDelete) {
                 $.ajax({
                    headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                    type: 'POST',
                    url : formUrl,
                    dataType: 'JSON',
                    data: {
                        '_token': '{{ csrf_token() }}',
                        '_method': 'DELETE',
                        'id': id,
                    },
                    success: function(res)
                    {
                        swal.stopLoading();
                        swal.close();
                        if(res.status == true)
                        {
                            toastr.success(res.message, 'Success');
                            location.reload();
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown)
                    {
                        swal.stopLoading();
                        swal.close();
                        toastr.error('Error deleted data', 'Error');
                    }
                });
            }
        });
    }
    </script>
@endsection
