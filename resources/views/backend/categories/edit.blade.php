@extends('layouts.app')

@section('title', 'Buat Kategori Buku')

@section('styles')
<link rel="stylesheet" href="{{ asset('modules/select2/css/select2.min.css') }}">
@endsection

@section('content')
<div class="section-header">
    <div class="section-header-back">
        <a href="{{ route('categories.index') }}" class="btn btn-icon"><i class="fas fa-arrow-left"></i></a>
    </div>
    <h1>Edit Kategori Buku</h1>
</div>

<div class="section-body">
    <div class="row">
        <div class="col-12">

            {!! Form::model($category, ['method' => 'PATCH', 'route' => ['categories.update', $category->id], 'autocomplete'=>'off', 'class'=> 'needs-validation', 'novalidate'=> '']) !!}
            <div class="card">
                <div class="card-body">

                    @if (count($errors) > 0)
                      <div class="alert alert-danger">
                        <strong>Whoops!</strong> There were some problems with your input.<br><br>
                        <ul>
                           @foreach ($errors->all() as $error)
                             <li>{{ $error }}</li>
                           @endforeach
                        </ul>
                      </div>
                    @endif

                    <div class="form-group row">
                        <label for="name" class="col-sm-3 col-form-label">Nama Kategori</label>
                        <div class="col-sm-9">
                            {!! Form::text('name', null, array('id' => 'name', 'placeholder' => 'Nama Kategori', 'class' => 'form-control', 'required', 'autofocus')) !!}
                        </div>
                    </div>

                </div>
                <div class="card-footer text-right">
                    <button type="submit" class="btn btn-lg btn-primary">Simpan</button>
                </div>
            </div>
            {!! Form::close() !!}

        </div>
    </div>
</div>
@endsection

@section('javascript')
    <script src="{{ asset('modules/select2/js/select2.min.js') }}"></script>
    <script src="{{ asset('modules/jquery-pwstrength/jquery.pwstrength.min.js') }}"></script>
    <script type="text/javascript">
    $(function(){
        @if ($message = Session::get('success'))
            toastr.success('{{ $message }}', 'Success');
        @endif

        $(".pwstrength").pwstrength();
    });
    </script>
@endsection
