@extends('layouts.app')

@section('title', 'Buat Kategori Buku')

@section('styles')
<link rel="stylesheet" href="{{ asset('modules/select2/css/select2.min.css') }}">
@endsection

@section('content')
<div class="section-header">
    <div class="section-header-back">
        <a href="{{ route('categories.index') }}" class="btn btn-icon"><i class="fas fa-arrow-left"></i></a>
    </div>
    <h1>Buat Kategori Buku</h1>
</div>

<div class="section-body">
    <div class="row">
        <div class="col-12">

            {!! Form::open(['method'=>'POST', 'route' => 'books.store', 'autocomplete'=>'off', 'class'=> 'needs-validation', 'novalidate'=> '']) !!}
            <div class="card">
                <div class="card-body">

                    @if (count($errors) > 0)
                      <div class="alert alert-danger">
                        <strong>Whoops!</strong> There were some problems with your input.<br><br>
                        <ul>
                           @foreach ($errors->all() as $error)
                             <li>{{ $error }}</li>
                           @endforeach
                        </ul>
                      </div>
                    @endif

                    <div class="form-group row">
                        <label for="name" class="col-sm-3 col-form-label">Nama Buku</label>
                        <div class="col-sm-9">
                            {!! Form::text('name', null, array('id' => 'name', 'placeholder' => 'Nama Buku', 'class' => 'form-control', 'required', 'autofocus')) !!}
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="type" class="col-sm-3 col-form-label">Tipe Kategori</label>
                        <div class="col-sm-9">
                           <select name="type" id="type" class="form-control" required>
                               @foreach ($categories as $category)
                                <option value="{{ $category->id }}">{{ $category->name }}</option>
                               @endforeach
                           </select>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="price" class="col-sm-3 col-form-label">Harga</label>
                        <div class="col-sm-9">
                            {!! Form::text('price', null, array('id' => 'price', 'placeholder' => 'Harga Buku', 'class' => 'form-control', 'required', 'autofocus')) !!}
                        </div>
                    </div>

                </div>
                <div class="card-footer text-right">
                    <button type="submit" class="btn btn-lg btn-primary">Simpan</button>
                </div>
            </div>
            {!! Form::close() !!}

        </div>
    </div>
</div>
@endsection

@section('javascript')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/cleave.js/1.6.0/cleave.min.js" integrity="sha512-KaIyHb30iXTXfGyI9cyKFUIRSSuekJt6/vqXtyQKhQP6ozZEGY8nOtRS6fExqE4+RbYHus2yGyYg1BrqxzV6YA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script type="text/javascript">
    $(function(){
        @if ($message = Session::get('success'))
            toastr.success('{{ $message }}', 'Success');
        @endif


        var cleave = new Cleave('#price', {
            numeral: true,
            numeralThousandsGroupStyle: 'thousand'
        });
    });
    </script>
@endsection
