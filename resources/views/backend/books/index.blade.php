@extends('layouts.app')

@section('title', 'Buku')

@section('styles')
<link rel="stylesheet" href="{{ asset('modules/datatables/datatables.min.css') }}">
<link rel="stylesheet" href="{{ asset('modules/datatables/datatables/css/dataTables.bootstrap4.min.css') }}">
@endsection

@section('content')
<div class="section-header">
    <h1>Buku</h1>

    @can('books-create')
        <div class="section-header-button ml-auto">
            <a class="btn btn-primary" href="{{ route('books.create') }}">
                Buat Buku
            </a>
        </div>
    @endcan
</div>

<div class="section-body">

    <div class="row">
        <div class="col-12">

            <div class="card">
                <div class="card-body">

                    <div class="table-responsive">
                        <table class="table table-striped" id="table-data">
                            <thead>
                                <tr>
                                    <th width="5%">#</th>
                                    <th>Nama Buku</th>
                                    <th>Kategori Buku</th>
                                    <th>Harga</th>
                                    <th width="20%">&nbsp;</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($books as $book)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $book->name }}</td>
                                        <td>{{ $book->dataCategory->name }}</td>
                                        <td>{{ "Rp " . number_format($book->price,0,',','.') }}</td>
                                        <td>
                                            <div class="btn-toolbar" role="toolbar">
                                                <div class="btn-group mr-2" role="group">
                                                    <a class="btn btn-primary" href="{{ route('books.edit', $book->id) }}">Edit</a>
                                                </div>
                                                <div class="btn-group">
                                                    {{ Form::button('Delete', ['id' => 'button-delete-'. $book->id, 'class' => 'btn btn-danger', 'data-route' => route('books.destroy', $book->id) , 'onclick' => 'delete_data('. $book->id .')']) }}
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>

                        </table>
                    </div>
                </div>
            </div>

        </div>
    </div>

</div>
@endsection

@section('javascript')
    <script src="{{ asset('modules/datatables/datatables/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('modules/datatables/datatables/js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('modules/sweetalert/sweetalert.min.js') }}"></script>
    <script type="text/javascript">
    var tableData;
    $(function() {
        @if ($message = Session::get('success'))
            toastr.success('{{ $message }}', 'Success');
        @endif

        tableData = $('#table-data').DataTable();

    });

    function delete_data(id)
    {
        var formUrl = $('#button-delete-'+ id).data('route');
        swal({
            title: 'Hapus Data?',
            text: 'Apakah yakin menghapus data?',
            buttons: {
                cancel: true,
                confirm: {
                    text: "Hapus!",
                    closeModal: false,
                }
            },
            dangerMode: true,
            closeOnClickOutside: false
        })
        .then((willDelete) => {
            if (willDelete) {
                 $.ajax({
                    headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                    type: 'POST',
                    url : formUrl,
                    dataType: 'JSON',
                    data: {
                        '_token': '{{ csrf_token() }}',
                        '_method': 'DELETE',
                        'id': id,
                    },
                    success: function(res)
                    {
                        swal.stopLoading();
                        swal.close();
                        if(res.status == true)
                        {
                            toastr.success(res.message, 'Success');
                            location.reload();
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown)
                    {
                        swal.stopLoading();
                        swal.close();
                        toastr.error('Error deleted data', 'Error');
                    }
                });
            }
        });
    }
    </script>
@endsection
