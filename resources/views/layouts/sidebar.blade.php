<div class="main-sidebar">
    <aside id="sidebar-wrapper">
        <div class="sidebar-brand">
            <a href="{{ route('dashboard') }}">
                <img src="{{ asset('images/logo-small.png')}}" alt="handlebrand">
                <span class="ml-1 text-primary">MT.com</span>
            </a>
        </div>
        <div class="sidebar-brand sidebar-brand-sm">
            <a href="{{ route('dashboard') }}">
                <img src="{{ asset('images/logo-small.png')}}" alt="handlebrand">
            </a>
        </div>
        <ul class="sidebar-menu">
            <li {!! request()->routeIs('dashboard') == true ? 'class="active"' : null !!}>
                <a class="nav-link" href="{{ route('dashboard') }}"><i class="fas fa-fire"></i> <span>Dashboard</span></a>
            </li>

            <li class="menu-header">Master Data</li>
            @can('books-list')
                <li {!! request()->routeIs('books*') == true ? 'class="active"' : null !!}>
                    <a href="{{ route('books.index') }}" class="nav-link"><i class="fas fa-book"></i> <span>Data Buku</span></a>
                </li>
            @endcan
            @can('categories-list')
                <li {!! request()->routeIs('categories*') == true ? 'class="active"' : null !!}>
                    <a href="{{ route('categories.index') }}" class="nav-link"><i class="fas fa-list-ul"></i> <span>Data Kategori</span></a>
                </li>
            @endcan


            @if(Auth::user()->can('users-list') || Auth::user()->can('roles-list'))
                <li class="menu-header">Pengaturan</li>
                    @can('users-list')
                        <li {!! request()->routeIs('users*') == true ? 'class="active"' : null !!}>
                            <a href="{{ route('users.index') }}" class="nav-link"><i class="far fa-id-badge"></i> <span>Administrator</span></a>
                        </li>
                    @endcan

                    @can('roles-list')
                        <li {!! request()->routeIs('roles*') == true ? 'class="active"' : null !!}>
                            <a href="{{ route('roles.index') }}" class="nav-link"><i class="fas fa-user-shield"></i> <span>Level Administrator</span></a>
                        </li>
                    @endcan
            @endif
        </ul>
    </aside>
</div>
